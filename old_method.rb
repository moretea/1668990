# routes.rb
TestApp::Application.routes.draw do
  resources :logins
  resource  :session

  resources :accounts do
    resources :things
  end
end

# action_controller.rb
class ApplicationController < ActionController::Base
  include LoginHelper # Force it to be present in all controllers
end

# login_helper.rb
module LoginHelper
  def current_login
    @current_login ||= Login.find_by_id(session[:login_id])
  end

  def logged_in?
    current_login.present?
  end
end

#things_controller.rb
class ThingsController < ApplicationController
  before_filter :find_account

  def find_account
    @account = Account.find(params[:account_id])
  end

  def index
    @things = @account.things
  end
end